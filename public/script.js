const SEARCH = document.querySelector("#projectsearch");
const CLEAR_BTN = document.querySelector("#projectsearchclear")

/**
 * Main function
 */
const main = () => {
    // check if search bar and its clear button exists
    if (SEARCH && CLEAR_BTN)
        searchbar();
};

/**
 * Manage search bar functions
 */
const searchbar = () => {
    console.log("Hello from searchbar");

    CLEAR_BTN.addEventListener("click", e => {
        console.log("Cleared search bar.")
        SEARCH.value = "";
    });

};

function filterFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('projectsearch');
    filter = input.value.toUpperCase();
    ul = document.getElementById("projects-list");
    li = ul.getElementsByClassName('project-item');

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("h3")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}



main();
